// Load required packages
var passport = require('passport');
var BasicStrategy = require('passport-http').BasicStrategy;
var BearerStrategy = require('passport-http-bearer').Strategy;
var User = require('../models/user');
var Client = require('../models/client');
var Token = require('../models/token');

passport.use(new BasicStrategy(
	function(username, password, callback) {
		User.findOne({ username: username }, function(error, user) {
			if (error) {
				return callback(error);
			}

			// No user found with that username
			if (!user) {
				return callback(null, false);
			}

			// Make sure the password is correct
			user.verifyPassword(password, function(error, isMatch) {
				if (error) {
					return callback(error);
				}

				// Password did not match
				if (!isMatch) {
					return callback(null, false);
				}

				// Succes
				return callback(null, user);
			});
		});
	}
));

passport.use('client-basic', new BasicStrategy(
	function(username, password, callback) {
		Client.findOne({ id: username }, function(error, client) {
			if (error) {
				return callback(error);
			}

			// No client found with that id or bad password
			if (!client || client.secret !== password) {
				return callback(null, false);
			}

			// Success
			return callback(null, client);
		});
	}
));

passport.use(new BearerStrategy(
	function(accessToken, callback) {
		Token.findOne({ value: accessToken }, function(error, token) {
			if (error) {
				return callback(error);
			}

			// No token found
			if (!token) {
				return callback(null, false);
			}

			User.findOne({ _id: token.userId }, function(error, user) {
				if (error) {
					return callback(err);
				}

				// No user found
				if (!user) {
					return callback(null, false);
				}

				// Simple example with no scope
				callback(null, user, { scope: '*' });
			});
		});
	}
));

exports.isAuthenticated = passport.authenticate(['basic', 'bearer'], { session : false });
exports.isClientAuthenticated = passport.authenticate('client-basic', { session : false });
exports.isBearerAuthenticated = passport.authenticate('bearer', { session: false });