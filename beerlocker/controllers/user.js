// Load required packages
var User = require('../models/user');

// Create endpoint /api/users for POST
exports.postUsers = function(request, response) {
	var user = new User({
		username: request.body.username,
		password: request.body.password
	});

	user.save(function(error) {
		if (error) {
			return response.send(error);
		}
		
		response.json({ message: 'New beer drinker added to th locker room!' });
	});
};

// Create endpoint /api/users for GET
exports.getUsers = function(request, response) {
	User.find(function(error, users) {
		if (error) {
			return response.send(error);
		}

		response.json(users);
	});
};