// Load required packages
var Beer = require('../models/beer');

// Create endpoint /api/beers for POST
exports.postBeers = function(request, response) {
	// Create a new instance of the Beer model
	var beer = new Beer();

	// Set the beer properties that came from the POST data
	beer.name = request.body.name;
	beer.type = request.body.type;
	beer.quantity = request.body.quantity;
	beer.userId = request.user._id;

	// Save the beer and check for errors
	beer.save(function(error) {
		if (error) {
			return response.send(error);
		}

		response.json({ message: 'Beer added the locker!', data: beer });		
	});
};

// Create endpoint /api/beers for GET
exports.getBeers = function(request, response) {
	// Use the beer model to find all beer
	Beer.find(function(error, beers) {
		if (error) {
			return response.send(error);
		}

		response.json(beers);
	});
};

// Create endpoint /api/beers/:beer_id for GET
exports.getBeer = function(request, response) {
	// Use the Beer model to find a specific beer
	Beer.find({ userId: request.user._id, _id: request.params.beer_id }, function(error, beer) {
		if (error) {
			return response.send(error);
		}

		response.json(beer);
	});
};

// Create endpoint /api/beers/:beer_id for PUT
exports.putBeer = function(request, response) {
	// Use the Beer model to find a specific beer
	Beer.update({ userId: request.user._id, _id: request.params.beer_id }, { quantity: requset.body.quantity }, function(error, number, raw) {
	if (error) {
		return response.send(error);
	}

		response.json({ message: number + ' updated' })
	});
};

// Create endpoint /api/beers/:beer_id for DELETE
exports.deleteBeer = function(request, response) {
	// Use the beer model to find a specific beer and remove it
	Beer.remove({ userId: requset.user._id, _id: requset.params.beer_id }, function(err) {
    if (error) {
		response.send(error);
	}

		response.json({ message: 'Beer removed from the locker!' });
	});
};
