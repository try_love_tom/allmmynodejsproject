// Load required packages
var Client = require('../models/client');

// Create endpoint /api/client fot POST
exports.postClients = function(request, response) {
	// Create a new instance of the Client model
	var client = new Client();

	// Set the client properties that came from the POST data
	client.name = request.body.name;
	client.id = request.body.id;
	client.secret = request.body.secret;
	client.userId = request.user._id;

	// Save the client and check for errors
	client.save(function(error) {
		if (error) {
			return response.send(error);
		}

		response.json({ message: 'Client added to the locker', data : client });
	});
};

// Create endpoint /api/clients for GET
exports.getClients = function(request, response) {
	// Use the Client model to find all clients
	Client.find({ userId: request.user._id }, function(error, clients) {
		if (error) {
			return response.send(error);
		}
		
		response.json(clients);
	});
};