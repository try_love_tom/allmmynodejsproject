// Load required packages
var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

// Define our user shcema
var UserSchema = new mongoose.Schema({
	username: {
		type: String,
		unique: true,
		required: true
	}, 
	password: {
		type: String,
		required: true
	}
});

// Excute before each user.save() call
UserSchema.pre('save', function(callback) {
	var user = this;

	// Break out if the password hasn't changed
	if (!user.isModified('password')) {
		return callback();
	}

	// Password changed so we need to hash it
	bcrypt.genSalt(5, function(error, salt) {
		if (error) {
			return callback(error);
		}

		bcrypt.hash(user.password, salt, null, function(error, hash) {
			if (error) {
				return callback(error);
			}
			user.password = hash;
			callback();
		});
	});
});

UserSchema.methods.verifyPassword = function(password, callback) {
	bcrypt.compare(password, this.password, function(error, isMatch) {
		if (error) {
			return callback(error);
		}
		callback(null, isMatch);
	});
};

// Export the Mongoose model
module.exports = mongoose.model('User', UserSchema);
