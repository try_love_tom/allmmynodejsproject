var steps = require( './steps' );
var phone = require( './phone' );
var pee = require( './pee' );

// 開始泡泡麵
steps.make_cup_noodles();

// ok 我現在是事件觸發模式所以我不用等泡麵泡好才能作其他事
// 但是我們現在多了一些新問題, 在同一個 scope 的函式幾乎是在同一時間觸發
phone.ring();

// 意思就是我有可能沒辦法接到這通電話
steps.answer_a_phone_call();

// 想尿尿
pee.explode();

// 還是有問題
steps.go_to_toilet();

// 我想吃泡麵但是已經被丟掉了
steps.eat_the_noodles();

// 一樣的錯誤在這裡發生, 還在煮的泡麵就這樣被丟掉了
// 所以到最後我一樣尿在褲子上也沒接到電話然後一樣沒吃到泡麵
steps.throw_the_cup_to_trash_can();