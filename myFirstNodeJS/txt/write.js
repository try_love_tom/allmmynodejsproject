var fs = require( 'fs' );

// exports i method for other modules or files to use
module.exports = {
	write : function( filename, data ) {
		// write to file synchromizely
		fs.writeFileSync( filename, data );
	}
}