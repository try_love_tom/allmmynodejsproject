var Hapi = require('hapi');
var server = new Hapi.Server();

server.connection({
	host: 'localhost',
	port: Number(process.argv[2] || 8080)
});

server.route({
	method: 'GET',
	path: '/',
	handler: function handler(request, reply) {
		// Request has all information
		// Reply handles client response

		reply('Hello hapi');
	}
})

server.start(function() { // boots your server
	console.log('Now Visit: http://localhost:8080')
});