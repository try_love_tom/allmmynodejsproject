var Hapi = require('hapi');
var Joi = require('joi');

var server = new Hapi.Server();

server.connection({
	host: 'localhost',
	port: Number(process.argv[2] || 8080)
});

server.route({
	method: 'GET',
	path: '/chicken/{breed}',
	config: {
		handler: function(request, reply) {
			reply('You asked for the chicken ' + request.params.breed);
		},
		validate: {
			params: {
				breed: Joi.string().required()
			}
		}
	}
});

server.start(function() { // boots your server
	console.log('Now Visit: http://localhost:8080/chicken/{breed}')
});