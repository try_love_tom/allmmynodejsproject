var Mongoose = require('../database').Mongoose;
var passportLocalMongoose = require('passport-local-mongoose');


// create the schema
var userSchema = new Mongoose.Schema({
	email: { type: String, require: true },
	password: { type: String, require: true },
	creationDate: { type: Date, require: true, default: Date.now },
});

userSchema.plugin(passportLocalMongoose, {
	userNameField: 'email',
	hashField: 'password',
	userNameLowerCase: true
});

// create the model
var User = Mongoose.model('User', userSchema, 'User');

exports.User = User;