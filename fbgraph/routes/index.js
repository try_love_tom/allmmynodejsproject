var express = require('express');
var router = express.Router();

// We need this to build our post string
var querystring = require('querystring');
var http = require('http');

/* GET home page. */
router.get('/', function(req, res, next) {
	res.render('index', {
		title: 'Express'
	});
});

// Create endpoint handlers for /namematch
var nameMatchController = require('../controllers/nameMatch');

router.route('/namematch')
	.post(nameMatchController.postNameMatch)
	.get(nameMatchController.getNameMatch);

module.exports = router;